const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const BlogPost = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    body: {
      type: Number,
      required: true,
    },
    image: {
      type: String,
      required: true,
    },
    hargabeli:{
      type: Number,
    },
    hargajual:{
      type: Number,
    },
    author: {
      type: Object,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("BlogPost", BlogPost);
